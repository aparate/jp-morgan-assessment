

 Implement a Small Message Processing Application

 The problem

    Implement a small message processing application that satisfies the below requirements for

    processing sales notification messages. You should assume that an external company will be sending

    you the input messages, but for the purposes of this exercise you are free to define the interfaces.

Processing requirements

    All sales must be recorded

    All messages must be processed

    After every 10th message received your application should log a report detailing the number of sales of each 
	product and their total value.

    After 50 messages your application should log that it is pausing, stop accepting new messages and log a report 
	of the adjustments that have been made to each sale type while the application was running.

Sales and Messages

    A sale has a product type field and a value – you should choose sensible types for these.

    Any number of different product types can be expected. There is no fixed set.

    A message notifying you of a sale could be one of the following types

    Message Type 1 – contains the details of 1 sale E.g apple at 10p

    Message Type 2 – contains the details of a sale and the number of occurrences of that 
	sale. E.g 20 sales of apples at 10p each.

    Message Type 3 – contains the details of a sale and an adjustment operation to be applied to all
	stored sales of this product type. Operations can be add, subtract, or multiply e.g Add 20p apples would instruct your application to add 20p to each sale of apples you have recorded.

 The solution created by me:

    The solution implements a ProcessSalesMessages class that is contained within a user defined package ProcessMessages,
     this package consists of other classes which helps to process the messages as desired in problem.

  ASSUMPTIONS: 

  1) One of the foremost assumption made from example is that the products of sale are fruits.
  2) The incoming messages are expected to be valid ones, invalid ones will be ignored. If the message has no format
     and are blank they will still be considered. 
  3) Another assumption made is messages of either message Type 2 format or message Type 3 format will be processed.
  4) Messages will be processed until 50 of them have been logged after 50 it will stop processing.
  5) After every 10th iteration the application logs a report of number of sales for each product and their 
     total value.
  

  LOGIC OF CODE PRODUCED:

     The class ProductDetails consist of all the necessary details about the product like it's type and cost. The 
	 messages are initially parsed for the details about the product. Once that is done all product details are stored 
	 in different objects which are than used to perform operations on the incoming messages by utilizing the product
	 information. The processed messages are than added to the sales log which is the class named Logs.
	 After every 10th iteration application logs a report.
	 
Software used: 
    NetBeans IDE 6.9


